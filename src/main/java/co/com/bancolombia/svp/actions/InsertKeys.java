package co.com.bancolombia.svp.actions;

import java.util.List;

import co.com.bancolombia.svp.userinterface.PasswordPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.bancolombia.svp.userinterface.PasswordPage.KEY;

public class InsertKeys implements Interaction{
	
    private char[] keys;
    
    public InsertKeys(String password) {
         this.keys = password.toCharArray();
    }
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		for(int i=0; i < keys.length ;i++) {
			String keyStr = String.valueOf(keys[i]);
			actor.attemptsTo(Click.on(KEY.of(keyStr)), Click.on(PasswordPage.TITLE_HEAD));
		}		
	}
	
	public static InsertKeys on(String password) {
		  return Tasks.instrumented(InsertKeys.class, password);
	}
	
}
