package co.com.bancolombia.svp.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;


import co.com.bancolombia.svp.questions.HomePageResult;
import co.com.bancolombia.svp.tasks.InsertPassword;
import co.com.bancolombia.svp.tasks.InsertUserInField;
import co.com.bancolombia.svp.tasks.OpenTheBrowser;
import co.com.bancolombia.svp.userinterface.LoginPage;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class LoginSvpStepDefinitions {

	@Managed
	private WebDriver driverBrowser;
	
	private Actor simon = Actor.named("simon");
	
	private LoginPage loginPage;
	
	@Before
	public void browseTheWeb() {
		simon.can(BrowseTheWeb.with(driverBrowser));
	}
	
	@Given("^Simon wants to login into SVP$")
	public void simonWantsToLoginIntoSVP() throws Exception {
	    simon.wasAbleTo(OpenTheBrowser.on(loginPage));
	}

	@When("^Simon inserts his credentials \"([^\"]*)\" y \"([^\"]*)\"$")
	public void simonInsertsHisCredentialsY(String username, String password) throws Exception {
        simon.attemptsTo(InsertUserInField.by(username), InsertPassword.by(password));
	}

	@Then("^he should see homepage of SVP$")
	public void heShouldSeeHomepageOfSVP() throws Exception {
         simon.should(seeThat(HomePageResult.is(), equalTo("Saldos por producto")));
	}
	
}
