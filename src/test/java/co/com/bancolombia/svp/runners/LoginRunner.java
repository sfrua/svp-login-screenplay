package co.com.bancolombia.svp.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/features/features/login.feature",glue="co.com.bancolombia.svp.stepdefinitions", snippets=SnippetType.CAMELCASE)
public class LoginRunner {

}
