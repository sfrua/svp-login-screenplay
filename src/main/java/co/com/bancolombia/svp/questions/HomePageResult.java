package co.com.bancolombia.svp.questions;

import co.com.bancolombia.svp.userinterface.HomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class HomePageResult implements Question<String>{

	@Override
	public String answeredBy(Actor actor) {

		return Text.of(HomePage.BALANCE_LBL).viewedBy(actor).asString();
	}
	
	public static HomePageResult is() {
		return new HomePageResult();
	}

}
