package co.com.bancolombia.svp.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class PasswordPage {
	public static final Target KEY = Target.the("key").locatedBy("//table[@id='_KEYBRD']//div[contains(text(),'{0}')]");
	public static final Target PASSWORD_FIELD = Target.the("password field").located(By.id("password"));
    public static final Target BUTTON_CONTINUE = Target.the("button continue").located(By.id("btnGo"));
    public static final Target TITLE_HEAD = Target.the("title head").located(By.xpath("//div[@id=\"header\"]/div[1]/div/div[2]"));
}
