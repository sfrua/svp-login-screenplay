package co.com.bancolombia.svp.tasks;

import co.com.bancolombia.svp.userinterface.LoginPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class InsertUserInField implements Task {

	private String username;
	
	public InsertUserInField(String username) {
		this.username = username;
	}
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(username).into(LoginPage.USERNAME_FIELD));
		actor.attemptsTo(Click.on(LoginPage.BUTTON_CONTINUE));
	}
	
	public static InsertUserInField by(String username) {
		return Tasks.instrumented(InsertUserInField.class, username);
	}
}
