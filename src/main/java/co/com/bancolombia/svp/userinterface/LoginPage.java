package co.com.bancolombia.svp.userinterface;


import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://bancolombia.qa5.todo1.com/cb/pages/jsp/home/mainPage.jsp")
public class LoginPage extends PageObject{
     public static final Target USERNAME_FIELD = Target.the("username field").located(By.id("username"));
     public static final Target BUTTON_CONTINUE = Target.the("button continue").located(By.id("btnGo"));
}
