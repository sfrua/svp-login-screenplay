package co.com.bancolombia.svp.tasks;

import co.com.bancolombia.svp.actions.InsertKeys;
import co.com.bancolombia.svp.userinterface.PasswordPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class InsertPassword implements Task{
	
	private String password;
	
	public InsertPassword(String password) {
		this.password = password;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(InsertKeys.on(password), Click.on(PasswordPage.BUTTON_CONTINUE));
	}
	
	public static InsertPassword by(String password) {
		   return Tasks.instrumented(InsertPassword.class, password);
	}

}
