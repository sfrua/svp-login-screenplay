package co.com.bancolombia.svp.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.IFrame;
import net.serenitybdd.screenplay.targets.Target;

public class HomePage {
    public static final Target BALANCE_LBL = Target.the("balance products").inIFrame(IFrame.withPath(By.id("ifrm"))).located(By.xpath("//h1[text() = 'Saldos por producto']"));
}
